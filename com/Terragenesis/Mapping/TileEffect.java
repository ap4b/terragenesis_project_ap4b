package com.Terragenesis.Mapping;

import com.Terragenesis.buildings.Building;

public class TileEffect {
	protected Building build;
	protected double effect;
	
	public TileEffect() {
		this.build=null;
		this.effect=1;
	}
	public TileEffect(Building build, double effect) {
		this.build=build;
		this.effect=effect;
	}	
	
	public Building getBuild() {
		return build;
	}
	public void setBuild(Building build) {
		this.build = build;
	}
	public double getEffect() {
		return effect;
	}
	public void setEffect(double effect) {
		this.effect = effect;
	}
	
	
	
	

}
