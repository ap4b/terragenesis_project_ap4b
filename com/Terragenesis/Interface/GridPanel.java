package com.Terragenesis.Interface;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JPanel;

import com.Terragenesis.Mapping.Map;

public class GridPanel extends JPanel{
	
	protected Map mapGrid;
	protected GridBagLayout gbl;
	protected GridBagConstraints gbc;	
	protected int width = 400;
	protected int height = 400;
	
	public GridPanel(Map map) {
		mapGrid = map;

		gbl = new GridBagLayout();
		gbc = new GridBagConstraints();
		this.setLayout(gbl);
		
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		
		for(int i = 0; i < mapGrid.getMap().size(); i++) {
			gbc.gridx = i;
			for(int j =0; j < mapGrid.getMap().get(0).size(); j++) {
				gbc.gridy = j;
				this.add(mapGrid.getMap().get(i).get(j), gbc);
			}
		}

	}
	
	public Map getMapGrid() {
		return mapGrid;
	}
}
