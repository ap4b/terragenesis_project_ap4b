package com.Terragenesis.Interface;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.Terragenesis.Planete.Atmosphere;
import com.Terragenesis.players.Player;

public class ActionPanel extends JPanel{
	protected int width = 1000;
	protected int height = 100;

	protected Player player;
	protected Atmosphere atmosphere;
	protected JLabel atmosphereLabel = new JLabel();
	protected JLabel actionPointLabel = new JLabel();
	protected JButton turnButton = new JButton("Tour Suivant");
	
	public ActionPanel(Player player, Atmosphere atmosphere){
		this.player = player;
		this.atmosphere = atmosphere;
		this.setBackground(Color.white);
		this.setLayout(new FlowLayout());
		this.setPreferredSize(new Dimension(width, height));
		atmosphereLabel.setPreferredSize(new Dimension(width/4, height));
		actionPointLabel.setPreferredSize(new Dimension(width/4, height));
		turnButton.setPreferredSize(new Dimension(width/4, height/2));
		
		this.add(atmosphereLabel);
		this.add(actionPointLabel);
		this.add(turnButton);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawRect(0, 0, width-1, height);
		atmosphereLabel.setText("<html>Atmosphere : <br>" + atmosphere.toString());
		actionPointLabel.setText("Points d'Action : " + player.getActionPoint() + "/" + player.getMaxActionPoint());
	}

	public void setAtmosphereLabel(String text) {
		atmosphereLabel.setText(text);
	}
	
	public JButton getTurnButton() {
		return turnButton;
	}
}
