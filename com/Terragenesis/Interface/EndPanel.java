package com.Terragenesis.Interface;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class EndPanel extends JPanel{
	JLabel endLabel = new JLabel();
	//protected JButton restartButton = new JButton("Recommencer");
	protected JButton closeButton = new JButton("Quitter le Jeu");

	public EndPanel() {

		this.setLayout(new FlowLayout());
		endLabel.setHorizontalAlignment(JLabel.CENTER);
		endLabel.setPreferredSize(new Dimension(2000, 500));
		this.add(endLabel);
		//this.add(restartButton);
		this.add(closeButton);
		}
	
	public JLabel getEndLabel() {
		return endLabel;
	}
	
//	public JButton getRestartButton() {
//		return restartButton;
//	}
	
	public JButton getCloseButton() {
		return closeButton;
	}
}
