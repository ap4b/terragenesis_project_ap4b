package com.Terragenesis.Interface;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;


import com.Terragenesis.Planete.Atmosphere;
import com.Terragenesis.buildings.*;
import com.Terragenesis.ressource.DrinkingWater;
import com.Terragenesis.ressource.Energy;
import com.Terragenesis.ressource.Iron;
import com.Terragenesis.ressource.Ressource;
import com.Terragenesis.ressource.Sand;
import com.Terragenesis.ressource.Wheat;

import javax.swing.JLabel;
import javax.swing.JList;

public class InfoPanel extends JPanel {  
	protected final int width = 300;		//taille de la fenetre
	protected final int height = 500;
	
	protected final int buttonWidth = width*3/4;	//taille des boutons
	protected final int buttonHeight = height/10;
	
	protected boolean isBuildState = false;
	protected boolean isConstructingState = false;
	
	protected Atmosphere atmosphere;
	
	protected FlowLayout fl = new FlowLayout();
	
	protected JLabel infoLabel = new JLabel("Informations");	//label "Information"
	protected JLabel ressourcesLabel = new JLabel();	//label d'information relatives aux ressources
	
	//listes et scroller des batiments possibles
	protected String buildingName[] = {"Batiment Culturel", "Batiment Scientifique" , "Batiment Energie",  
			"Batiment Hydrolique", "Carriere de Sable", "Batiment Minage", "Batiment d'Atmosphere", "Batiment d'Agriculture"};
	protected JList<String> buildingList = new JList<String>(buildingName);
	protected JScrollPane buildingScroller = new JScrollPane(buildingList, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
	protected JLabel buildingConstructionInfoLabel = new JLabel();	//information sur le batiment s�lectionn�
	
	protected JLabel buildingInfoLabel = new JLabel();
	
	protected JSpinner popAllocationSpinner = new JSpinner();
	
	protected JButton constructButton = new JButton("Construire (-2PA)");	//boutons de construction et d'allocation
	protected JButton alocateButton = new JButton("Allouer");
	protected JButton levelUpButton = new JButton("Am�liorer (-2PA)");
	
	
	public InfoPanel(Atmosphere atmosphere){
		
		this.atmosphere = atmosphere;
		this.setPreferredSize(new Dimension(width, height));	//mise en page du panneau
		this.setBackground(Color.white);
		this.setLayout(fl);
		fl.setVgap(10);
		
		infoLabel.setHorizontalAlignment(JLabel.CENTER);	//mise en page du label "Informations"
		infoLabel.setPreferredSize(new Dimension(buttonWidth, height/20));
		this.add(infoLabel);
		
		ressourcesLabel.setHorizontalAlignment(JLabel.LEFT);	//mise en page du label des ressources
		ressourcesLabel.setPreferredSize(new Dimension(buttonWidth,height/3));
		this.add(ressourcesLabel);
		
		//mise en page du scroller des types de batiments
		buildingList.addListSelectionListener(e -> {if(!buildingList.isSelectionEmpty())selectedBuildingInfo(buildingList.getSelectedValue().toString());	//on change les informations relatives au batiment
													constructButton.setVisible(true);	//on montre le bouton de construction et les information du batiment
													buildingConstructionInfoLabel.setVisible(true);});
		
		buildingScroller.setPreferredSize(new Dimension(buttonWidth, height/8));
		this.add(buildingScroller);
		
		buildingConstructionInfoLabel.setPreferredSize(new Dimension(buttonWidth, height/5));		//mise en page des informations relatives au batiment selection�
		buildingConstructionInfoLabel.setVisible(false);
		this.add(buildingConstructionInfoLabel);
		
		buildingInfoLabel.setPreferredSize(new Dimension(buttonWidth, height/6));		//mise en page des informations relatives au batiment de la case
		buildingInfoLabel.setVisible(true);
		this.add(buildingInfoLabel);
		
		constructButton.setPreferredSize(new Dimension(buttonWidth, buttonHeight));	//mise en page du bouttons construire
		constructButton.setVisible(false);
		this.add(constructButton);
		
		popAllocationSpinner.setPreferredSize(new Dimension(buttonWidth, height/20)); //mise en page du spinner de population � allouer
		popAllocationSpinner.setVisible(false);
		this.add(popAllocationSpinner);
		
		alocateButton.setPreferredSize(new Dimension(buttonWidth, buttonHeight));	//mise en page des bouttons allouer et ameliorer
		alocateButton.setVisible(false);
		this.add(alocateButton);
		
		levelUpButton.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
		levelUpButton.setVisible(false);
		this.add(levelUpButton);
	}
	
	public void paintComponent(Graphics g) {	//on dessine le tour de la fenetre
		super.paintComponent(g);
			g.setColor(Color.black);
			g.drawRect(0, 0, width-1, height-1);
			
			if(isBuildState) {					//on affiche les �l�ments en focntion de la pr�sence de batiment sur la case
				constructButton.setVisible(false);
				buildingScroller.setVisible(false);
				buildingConstructionInfoLabel.setVisible(false);
				
				buildingInfoLabel.setVisible(true);
				alocateButton.setVisible(true);		
				levelUpButton.setVisible(true);
				popAllocationSpinner.setVisible(true);
				
				
			}else if(isConstructingState) {
				constructButton.setVisible(false);
				buildingScroller.setVisible(false);
				buildingConstructionInfoLabel.setVisible(false);
				alocateButton.setVisible(false);	
				levelUpButton.setVisible(false);
				popAllocationSpinner.setVisible(false);
				
				buildingInfoLabel.setVisible(true);
				
			}else {
				buildingScroller.setVisible(true);
				
				popAllocationSpinner.setVisible(false);
				buildingInfoLabel.setVisible(false);
				alocateButton.setVisible(false);	
				levelUpButton.setVisible(false);
			}
			
	}
	
	public Building selectedBuilding() {	//renvoie le batiment selectionn�
		Building selectedBuilding;
		switch(buildingList.getSelectedValue().toString()) {
			case "Batiment Culturel" :
				selectedBuilding = new CulturalBuilding();
				break;
			case "Batiment Energie" :
				selectedBuilding = new EnergyBuilding(new Energy());
				break;
			case "Batiment Hydrolique" :
				selectedBuilding = new HydraulicBuilding(new DrinkingWater());
				break;
			case "Carriere de Sable" :
				selectedBuilding = new MiningBuilding(new Sand());
				break;
			case "Batiment Minage" :
				selectedBuilding = new MiningBuilding(new Iron());
				break;
			case "Batiment Scientifique" :
				selectedBuilding = new ScientificBuilding();
				break;
			case "Batiment d'Atmosphere" :
				selectedBuilding = new AtmosphereBuilding(atmosphere);
				break;
			case "Batiment d'Agriculture":
				selectedBuilding = new AgriculturalBuilding(new Wheat());
				break;
			default : selectedBuilding = new MiningBuilding(new Iron());
		}
		return selectedBuilding;
	}
	
	protected void selectedBuildingInfo(String building) {	//mets � jour les information du batiment s�l�ctionn� //�modifier(ajouter une description a chaques batiments)
		String text = new String("<html>Description :<br>");
		switch(building) {
			case "Batiment Culturel" :
				text += "<p>Ce batiment permet de faire progresser le niveau de culture<p>"
						+ "<br>Co�t : " + buildingCost(new CulturalBuilding()) 
						+ "<br>Temps de construction : " + new CulturalBuilding().getConstructionTime();
				break;
			case "Batiment Energie" :
				text += "<p>Ce batiment permet de cr�er de l'Energie<p>"
						+ "<br>Co�t : " + buildingCost(new EnergyBuilding()) 
						+ "<br>Temps de construction : " + new EnergyBuilding().getConstructionTime();
				break;
			case "Carriere de Sable" :
				text += "<p>Ce batiment permet de r�colter du sable<p>"
						+ "<br>Co�t : " + buildingCost(new MiningBuilding()) 
						+ "<br>Temps de construction : " + new MiningBuilding().getConstructionTime();
				break;
			case "Batiment Hydrolique" :
				text += "<p>Ce batiment permet de r�colter l'Eau pr�sente sur la case<p>"
						+ "<br>Co�t : " + buildingCost(new HydraulicBuilding()) 
						+ "<br>Temps de construction : " + new HydraulicBuilding().getConstructionTime();
				break;
			case "Batiment Minage" :
				text += "<p>Ce batiment permet de r�coter le Fer pr�sent sur la case<p>"
						+ "<br>Co�t : " + buildingCost(new MiningBuilding()) 
						+ "<br>Temps de construction : " + new MiningBuilding().getConstructionTime();
				break;
			case "Batiment Scientifique" :
				text += "<p>Ce batiment permet de faire progresser le niveau de Science<p>"
						+ "<br>Co�t : " + buildingCost(new ScientificBuilding()) 
						+ "<br>Temps de construction : " + new ScientificBuilding().getConstructionTime();
				break;
			case "Batiment d'Atmosphere" :
				text +=	"<p>Ce batiment permet d'am�liorer la qualit� de l'Amosphere<p>"
						+ "<br>Co�t : " + buildingCost(new AtmosphereBuilding(atmosphere)) 
						+ "<br>Temps de construction : " + new AtmosphereBuilding(atmosphere).getConstructionTime();
				break;
			case "Batiment d'Agriculture":
				text += "<p>Ce batiment permet de r�colter de la nourriture<p>"
						+ "<br>Co�t : " + buildingCost(new AgriculturalBuilding()) 
						+ "<br>Temps de construction : " + new AgriculturalBuilding().getConstructionTime();
				break;
			default : text += "<p>Building not foung or notImplemented yet<p>";
		}
		buildingConstructionInfoLabel.setText(text);
	}
	
	protected String buildingCost(Building building) {
		String text = new String();
		for(Ressource r : building.getNeededRessource()) {
			text += r.getName() + " : " + r.getNumber() + " ";
			}
		return text;
	}
	
	//getteurs et setteurs
	public JButton getConstructButton() {
		return constructButton;
	}
	
	public JButton getAlocateButton() {
		return alocateButton;
	}
	
	public JButton getLevelUpButton() {
		return levelUpButton;
	}
	
	public JLabel getRessourcesLabel() {
		return ressourcesLabel;
	}
	
	public void setRessourcesLabel(String text) {
		ressourcesLabel.setText(text);
	}
	
	public void setBuildingInfoLabel(String text) {
		buildingInfoLabel.setText(text);
	}
	
	public void setIsBuildState(boolean bool) {
		isBuildState = bool;
	}
	
	public void setIsConstructingState(boolean bool) {
		isConstructingState = bool;
	}
	
	public JList<String> getBuildingList() {
		return buildingList;
	}
	
	public JSpinner getPopSpinner() {
		return popAllocationSpinner;
	}
	
	public JLabel getBuildingConstructionInfoLabel() {
		return buildingConstructionInfoLabel;
	}
}
