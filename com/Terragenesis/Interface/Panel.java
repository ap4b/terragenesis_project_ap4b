package com.Terragenesis.Interface;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import com.Terragenesis.Mapping.Map;
import com.Terragenesis.Mapping.Tile;
import com.Terragenesis.Planete.Atmosphere;
import com.Terragenesis.Planete.Planete;
import com.Terragenesis.buildings.Building;
import com.Terragenesis.buildings.MiningBuilding;
import com.Terragenesis.players.Player;
import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.DrinkingWater;
import com.Terragenesis.ressource.Energy;
import com.Terragenesis.ressource.Iron;
import com.Terragenesis.ressource.Ressource;
import com.Terragenesis.ressource.Sand;
import com.Terragenesis.ressource.Wheat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;


public class Panel extends JPanel implements MouseListener{
	protected final int width = 1000;
	protected final int height = 700;
	protected final int winPop = 100000;
	ArrayList<Ressource> list= new ArrayList<Ressource>();

	protected Planete planete = new Planete();
	protected Player player = planete.getPlayer();
	protected Stat stat = player.getPlayerStat();
	protected Atmosphere atmosphere = planete.getAtmosphere();
	
	protected GridPanel gridPanel = new GridPanel(planete.getMap());
	protected InfoPanel infoPanel = new InfoPanel(planete.getAtmosphere());
	protected ActionPanel actionPanel = new ActionPanel(player, planete.getAtmosphere());
	protected StatPanel statPanel;
	
	protected EndPanel endPanel = new EndPanel();
	
	protected BorderLayout bl = new BorderLayout();

	protected int activeX, activeY;
	
	public Panel() {
		//this.setPreferredSize(new Dimension(width, height));

		statPanel = new StatPanel(stat);
				
		bl.setVgap(0);
		this.setLayout(bl);
		this.setBorder(null);
		this.addMouseListener(this);
		this.add("East", infoPanel);
		this.add("North", statPanel);
		this.add("West", gridPanel);
		this.add("South", actionPanel);
		this.add("Center", endPanel);
		
		endPanel.setVisible(false);
		
		actualizeSpinner();
		
		infoPanel.getConstructButton().addActionListener(e -> {getActive().setIsConstructing(true);
																planete.constructBuilding(activeX, activeY, infoPanel.selectedBuilding());
																player.useAction(2);
																planete.getAtmosphere().makeBetter();});
		
		infoPanel.getAlocateButton().addActionListener(e -> getActive().allocatePop((int) infoPanel.getPopSpinner().getValue(), stat));
		
		infoPanel.getLevelUpButton().addActionListener(e -> {getActive();
															planete.LevelUp(activeX, activeY);
															player.useAction(2);});
		
		actionPanel.getTurnButton().addActionListener(e -> planete.turn());
		
		infoPanel.getPopSpinner().addChangeListener(e -> actualizeSpinner());	
		
		
		}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		 
	}
	@Override
	public void mousePressed(MouseEvent e) {
		
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	

		public void paintComponent(Graphics g) {	//affcihe la zone graphique
			super.paintComponent(g);
//			statPanel.setStat(stat);
			
			if(this.isLose()) {
				endPanel.getEndLabel().setText("<html>Toute votre population est d�c�d�...<br>Vous avez Perdu");
				actionPanel.setVisible(false);
				statPanel.setVisible(false);
				Tile.setSelected(false);
				infoPanel.setVisible(false);
				gridPanel.setVisible(false);
				endPanel.setVisible(true);
			}else if(this.isWin()) {
				endPanel.getEndLabel().setText("Bravo ! Vous avez Gagne");
				actionPanel.setVisible(false);
				statPanel.setVisible(false);
				Tile.setSelected(false);
				infoPanel.setVisible(false);
				gridPanel.setVisible(false);
				endPanel.setVisible(true);
			}
			
			
			
			
			if(Tile.getSelected())		//si une case est activ�e
			{
				Tile activeTile = getActive();		//on r�cupere la case
				
				
				if(activeTile.getIsBuild()) {
					infoPanel.setBuildingInfoLabel("<html>" + buildingText(activeTile));
					infoPanel.setIsBuildState(true);
					if(player.getActionPoint()<2) infoPanel.getLevelUpButton().setEnabled(false);
					
				}else if(activeTile.getIsConstructing()){
					if(!planete.getMap().getMap().get(activeX).get(activeY).getConstruction().isConstructing()) {
						actualizeSpinner();
						activeTile.setIsBuild(true);
					}
					
					infoPanel.setIsConstructingState(true);
					infoPanel.setBuildingInfoLabel("Tour avant fin : " + activeTile.getConstruction().getRemainigTime());
					
				}else {
					infoPanel.setIsBuildState(false);
					infoPanel.setIsConstructingState(false);
					if(player.getActionPoint()<2 || (!planete.checkCanConstruct(activeX, activeY, new MiningBuilding()))) infoPanel.getConstructButton().setEnabled(false);
				}
				
				infoPanel.setRessourcesLabel("<html>" /*"case x : " + activeX + " y : " + activeY*/ +	//on r�cupere les information des ressources
							"<br>Biome : " + activeTile.getBiome() + "<br><br>Ressources : <br><br><p> " + ressourceText(activeTile) + "<p>");
				infoPanel.setVisible(true);	//on affiche le paneau d'informations
			}else {
				infoPanel.setVisible(false);	//si aucune case n'est activ�e -> pas de panneau d'information
				infoPanel.getBuildingList().clearSelection();  //on reset la selection de liste
				infoPanel.getConstructButton().setVisible(false);
				infoPanel.getBuildingConstructionInfoLabel().setVisible(false);
			}
		}
			
			
		public Tile getActive() {	//r�cup�re la case active
			if(Tile.getSelected())
				for(int i = 0; i < gridPanel.getMapGrid().getMap().size(); i++)	//on parcout le tableau de case 
				{
					for(int j = 0; j < gridPanel.getMapGrid().getMap().get(0).size(); j++) {
						if(gridPanel.getMapGrid().getMap().get(i).get(j).getActive()) {	//si la case parcourue est active
							this.activeX = i;
							this.activeY = j;	
							return gridPanel.getMapGrid().getMap().get(i).get(j);	//on r�cup�re la case
						}
					}
				}
			return null;
		}
		
		public String ressourceText(Tile tile) {	//r�cup�re le texte d'information des ressources
			String text = new String();
			for(Ressource ressource : tile.getRessource()) {	//pour chaque ressource 
				text = text + ressource.getName() + " : " + ressource.getNumber() + "<br>";	//on r�cup�re son nom et son nombre
			}
			return text;
			
		}
		
		public String buildingText(Tile tile) {
			Building building = tile.getConstruction();
			String text = new String();
			text = "Batiment : <br>Type : " + building.getType();
			if(building.getType() == "Minage")
				text += " " + building.getTypeOfBuildingYield();
			text += "<br>Niveau : " + building.getActualLevel() 
					+ "<br>Popultion allou�e : " + building.getNumberPopulation() + "<br>Cout d'augmentation : " + building.getCostLevelUp()
					+ "<br><br>Population Disponible : " + (stat.getPop().getNumberPopulation() - Tile.getTotalAllocatePopulation());
			return text;
		}
		
		protected void actualizeSpinner() {
			infoPanel.getPopSpinner().setModel(
					new SpinnerNumberModel((Number) infoPanel.getPopSpinner().getValue(), 0, 
							stat.getPop().getNumberPopulation() - Tile.getTotalAllocatePopulation(), 100));
		}
		
		protected boolean isWin()
		{
			return (stat.getPop().getNumberPopulation() >= winPop && atmosphere.isGood());
		}
		
		protected boolean isLose()
		{
			return stat.getPop().getNumberPopulation() <= 0;
		}
		
		public EndPanel getEndPanel() {
			return endPanel;
		}
}
