package com.Terragenesis.Interface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.DrinkingWater;
import com.Terragenesis.ressource.Energy;
import com.Terragenesis.ressource.Iron;
import com.Terragenesis.ressource.Ressource;
import com.Terragenesis.ressource.Sand;
import com.Terragenesis.ressource.Wheat;

public class StatPanel extends JPanel{

	protected int width = 1000;
	protected int height = 100;
	
	
	protected Stat stat;
	protected JLabel statLabel = new JLabel("Statistiques :");
	
	protected BorderLayout bl = new BorderLayout();
	protected JPanel subPanel = new JPanel();
	protected FlowLayout fl = new FlowLayout();
	
	protected JLabel scienceLabel = new JLabel();
	protected JLabel cultureLabel = new JLabel();
	protected JLabel popLabel = new JLabel();
	protected JLabel hapinessLabel = new JLabel();
	//protected JLabel energyLabel = new JLabel();
	//protected JLabel totalFoodLabel = new JLabel();
	protected JLabel waterConsumptionLabel = new JLabel();
	protected JLabel foodConsumptionLabel = new JLabel();
	protected JLabel ressourcesLabel = new JLabel();
	
	public StatPanel(Stat stat){
		
		this.stat = stat;
		
		this.setPreferredSize(new Dimension(width, height));
		this.setVisible(true);
		this.setBackground(Color.white);
		this.setBorder(new EmptyBorder(0,10,10,10));
		this.setLayout(bl);
		bl.setVgap(20);
		
		statLabel.setHorizontalAlignment(JLabel.CENTER);
		this.add("North", statLabel);
		fl.setHgap(20);
		subPanel.setLayout(fl);
		
		
		subPanel.add(scienceLabel);
		subPanel.add(cultureLabel);
		subPanel.add(popLabel);
		subPanel.add(hapinessLabel);
		//subPanel.add(energyLabel);
		//subPanel.add(totalFoodLabel);
		subPanel.add(waterConsumptionLabel);
		subPanel.add(foodConsumptionLabel);
		subPanel.add(ressourcesLabel);
		
		subPanel.setBackground(Color.white);
		this.add("Center", subPanel);
		this.setVisible(true);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.black);
		g.drawRect(0, 0, width-1, height-1);
		scienceLabel.setText("Science : " + stat.getScience());
		cultureLabel.setText("Culture : " + stat.getCulture());
		popLabel.setText("Population : " + stat.getPop().getNumberPopulation());
		hapinessLabel.setText("Joie : " + stat.getPop().getHappiness());
		//energyLabel.setText("Energie : " + stat.getEnergy());
		//totalFoodLabel.setText("Nourriture : " + stat.getRessourceStat().get(stat.getRessourceIndex(new Wheat())).getNumber());
		waterConsumptionLabel.setText("Consommation d' eau : " + stat.getPop().getDrinkingWaterConsumption());
		foodConsumptionLabel.setText("Consommation de nourriture : " + stat.getPop().getFoodconsumption());
		
		ressourcesLabel.setText("Ressources :   " + ressourceStatText());
		
	}
	
	protected String ressourceStatText() {
		String text = new String();
		for(Ressource ressource : stat.getRessourceStat()) {	//pour chaque ressource 
				text += ressource.getName() + " : " + ressource.getNumber() + "    ";	//on r�cup�re son nom et son nombre
			}
		
		return text;
	}
	
	public void setStat(Stat stat) {
		this.stat = stat;
	}
	
	public Stat getStat() {
		return stat;
	}
	
	
	
	
}
