package com.Terragenesis.Interface;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class Cell extends JPanel implements MouseListener{
	protected static boolean selected = false;
	protected static int width = 40;
	protected static int height = 40;
	protected final static String desert = "#E4AB6B";
	protected final static String plain = "#6F8D48";
	protected final static String toundra = "#72250E";
	protected final static String sea = "#2E71CF";
	protected final static String defaultColor = "#ffffff";
	
	protected String buildingColor = "#B8B8B8";
	protected String color = defaultColor;
	protected String enteredColor = "#e8e8e8";
	protected String activeColor = "#a7a7a7";
	protected String biome;
	protected boolean active = false;
	protected boolean entered = false;
	protected boolean isBuild = false;
	protected boolean isConstructing = false;

	
	public Cell(){
		this.setPreferredSize(new Dimension(width, height));
		this.setBackground(Color.blue);
		this.addMouseListener(this);
		
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.decode(color));
		g.fillRect(0, 0, width, height);
		g.setColor(Color.black);
		g.drawRect(0, 0, width-1, height-1);
		
		if(!selected) {
			active = false;
		}
		if(entered)
		{
			g.setColor(Color.decode(enteredColor));
			g.fillRect(1, 1, width-1, height-1);
		}
		if(active) {
			g.setColor(Color.decode(activeColor));
			g.fillRect(1, 1, width-1, height-1);
		}
		if(isBuild || isConstructing) {
			g.setColor(Color.decode(buildingColor));
			g.fillOval(width/4, height/4, width/2, height/2);
		}
		
	}
	
	public void mouseEntered(MouseEvent event) {
			entered = true;
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		if(!selected) {
			active = true;
			selected = true;
		}else  
			selected = false;
	}
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		entered = false;
		
	}
	public static boolean getSelected() {
		return selected;
	}
	
	public boolean getActive() {
		return active;
	}
	
	public void setColor(int area) {
		switch(area) {
		case 0 :
			color = desert;
			break;
		case 1 :
			color = toundra;
			break;
		case 2 :
			color = plain;
			break;
		case 3 :
			color = sea;
			break;
		default : 
			color = defaultColor;
		}
	}
	
	public void setIsBuild(boolean bool) {
		isBuild = bool;
	}
	
	public boolean getIsBuild() {
		return isBuild;
	}
	
	public void setIsConstructing(boolean bool) {
		isConstructing = bool;
	}
	
	public boolean getIsConstructing() {
		return isConstructing;
	}
	
	public void setBiome(String biome) {
		this.biome = biome;
	}
	
	public String getBiome() {
		return biome;
	}
	
	public static void setSelected(boolean bool) {
		selected = false;
	}
}

