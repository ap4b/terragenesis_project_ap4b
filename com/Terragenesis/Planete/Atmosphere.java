package com.Terragenesis.Planete;

import com.Terragenesis.Mapping.Map;

public class Atmosphere {
	
	
	protected double diAzote;
	protected double diOxygene;
	protected double dioxydeCarbonne;
	protected double monoxydeDihydrogene; //eau
	
	public Atmosphere(){
		this.diAzote=0;
		this.dioxydeCarbonne=0;
		this.diOxygene=0;
		this.monoxydeDihydrogene=0;
		this.GenerateEasyAtmosphere();
	}

	public void randomGenereratingAtmosphere() {
		
		this.dioxydeCarbonne=Math.random()*100;
		this.monoxydeDihydrogene= (100-this.dioxydeCarbonne);
		
		
		this.diOxygene=Math.random()*100;
		this.diAzote= (100-this.diOxygene);
	}
	
	public void GenerateEasyAtmosphere() {
		diAzote = 40 + (Math.random() * (40));
		diOxygene = 5 + (Math.random() * ((100-diAzote) - 5));
		monoxydeDihydrogene = 1 + (Math.random() * ((95-diAzote - diOxygene) - 1));
		dioxydeCarbonne =  5 + (Math.random() * ((100-diAzote - diOxygene) - 5));
		}
	
	public void MapGeneratingAtmosphere(Map m) {
		
		double diAzote;
		double diOxygene;
		double dioxydeCarbonne;
		double monoxydeDihydrogene;
		
		
	}
	
	
	
	public String toString() {
		return "N2 : " + diAzote + "<br>CO2 : " + dioxydeCarbonne +"<br>O2 : " + diOxygene + "<br>H2O : " + monoxydeDihydrogene;
	}
	
	public boolean isGood() {
		return diAzote > 65 && diAzote < 85 && diOxygene > 20 && diOxygene < 30 && dioxydeCarbonne < 2 && dioxydeCarbonne > 0 && monoxydeDihydrogene < 5 && monoxydeDihydrogene > 0;
	}
	
	public boolean isGoodO2() {
		return diOxygene > 20 && diOxygene < 30 ;
	}

	public boolean isGoodCO2() {
		return dioxydeCarbonne < 2 && dioxydeCarbonne > 0;
	}
	
	public boolean isGoodH2O() {
		return  monoxydeDihydrogene < 5 && monoxydeDihydrogene > 0;
	}
	
	public boolean isGoodN2() {
		return diAzote > 70 && diAzote < 80;
	}

	public void makeBetter() {
		
		if(!this.isGoodCO2())
			if (dioxydeCarbonne > 2)
			{
				dioxydeCarbonne -= 1;
				diAzote += 1; 
			}
		
		if(!this.isGoodH2O())
			if (monoxydeDihydrogene > 5)
			{
				monoxydeDihydrogene -= 1.5;
				diAzote += 1.5; 
			}
		
		if(!this.isGoodO2())
			if (diOxygene > 30)
			{
				diOxygene -= 2;
				diAzote += 2; 
			}
			else {
				diOxygene += 2;
				diAzote -= 2; 
			}
				
	}

	public void setH2O(float nb) {
		monoxydeDihydrogene = nb;
	}


}
