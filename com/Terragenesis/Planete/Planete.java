package com.Terragenesis.Planete;

import com.Terragenesis.Mapping.Map;
import com.Terragenesis.buildings.Building;
import com.Terragenesis.players.Player;
import com.Terragenesis.players.Stat;

public class Planete {
	
	Map map;
	Player player;
	Atmosphere atmosphere;
	
	public Planete(){
		
		map = new Map();
		player = new Player();
		atmosphere = new Atmosphere();
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void constructBuilding(int x, int y, Building built) {
		
		this.map.build(x, y, built, this.player.getPlayerStat());
	}
//--------------------------------------------------------------------------------------------------------------------------------------------
	public void destroyBuilding(int x, int y) {
		
		this.map.getMap().get(x).get(y).destroy();

	}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	private void harvestBuilding() {
		this.map.harvestBuilding( this.player.getPlayerStat() );
		
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public boolean checkCanConstruct(int x, int y,Building bat) {
		return this.map.checkCanConstruct( x, y, bat, this.player.getPlayerStat() );
	
	}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public boolean checkCanLevelUp(int x, int y,Building bat ) {  // allocate pop
		
		return this.map.checkCanLevelUp(x, y, bat, this.player.getPlayerStat());
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void LevelUp(int x, int y ) {  // allocate pop
		
		if(  this.checkCanLevelUp(x, y, this.map.getMap().get(x).get(y).getConstruction() ) ){
			
			this.map.levelUpBuilding(this.player.getPlayerStat(), x, y);
			
		}
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void allocatePop(int x, int y, int pop) {
		
		this.map.allocationPop(x, y, this.player.getPlayerStat(), pop);
	}
	
	
	
	public void turn() {
		
		this.harvestBuilding();
		this.player.getPlayerStat().evolvePopulation();
		player.getPlayerStat().PopulationConsuming();
		this.map.desallocatePop( this.player.getPlayerStat() );
		
		player.resetActionPoints();
		}
	
	public Player getPlayer() {
		return player;
	}
	
	public Map getMap() {
		return map;
	}
	
	public Atmosphere getAtmosphere() {
		return atmosphere;
	}
	
public void  showMap() {
		
		this.map.showMap();
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void showPop() {
		
		System.out.printf("\n");
		System.out.print(this.player.getPlayerStat().getPop().toString());
		System.out.printf("\n");

	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public void showStatRessource() {
		
		System.out.printf("\n");
		System.out.print(this.player.getPlayerStat().getRessourceStat().toString());
		System.out.printf("\n");

	}

}
