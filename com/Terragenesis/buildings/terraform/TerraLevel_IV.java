package com.Terragenesis.buildings.terraform;

public class TerraLevel_IV implements Terraform{

	public double terraform() {
		return 4*Terraform.num;
		
	}
	
	public double getCostLevelUp() {
		return 4*Terraform.costLevel;
	}
}
