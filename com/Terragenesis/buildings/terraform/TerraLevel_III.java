package com.Terragenesis.buildings.terraform;

public class TerraLevel_III implements Terraform{

	public double terraform() {
		return 3*Terraform.num;
		
	}
	
	public double getCostLevelUp() {
		return 3*Terraform.costLevel;
	}
}
