package com.Terragenesis.buildings.terraform;

public interface Terraform {
	
	static double num=10;
	static double costLevel=50;
	
	public double terraform();
	public double getCostLevelUp();
}
