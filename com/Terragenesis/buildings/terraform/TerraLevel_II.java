package com.Terragenesis.buildings.terraform;

public class TerraLevel_II implements Terraform{

	public double terraform() {
		return 2*Terraform.num;
		
	}
	
	public double getCostLevelUp() {
		return 2*Terraform.costLevel;
	}
}
