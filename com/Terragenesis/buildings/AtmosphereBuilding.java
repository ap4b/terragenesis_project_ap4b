package com.Terragenesis.buildings;

import com.Terragenesis.Mapping.Tile;
import com.Terragenesis.Planete.Atmosphere;
import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.Ressource;

public class AtmosphereBuilding extends TerraformingBuilding {
	
	protected Atmosphere atmosphere;
	
	 public AtmosphereBuilding(Atmosphere atmosphere)
		{
		 	this.type = "Atmosphere";
			this.costLevelUp=400;
			this.numberPopulation=0;
			this.constructionTime=3;
			this.remainingTime=this.constructionTime;
			this.costLevelUp=this.level.getCostLevelUp();
			this.atmosphere = atmosphere;
			
		}

	@Override
	public void doBuildingAction(Stat stat, Tile tile) {
		System.out.println("amosphere");
		atmosphere.makeBetter();
	}

	
}
