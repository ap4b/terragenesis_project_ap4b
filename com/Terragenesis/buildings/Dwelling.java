package com.Terragenesis.buildings;

import java.util.ArrayList;

import com.Terragenesis.Mapping.Tile;
import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.Iron;
import com.Terragenesis.ressource.Ressource;
import com.Terragenesis.ressource.Sand;

public class Dwelling extends Building{
	
	protected int PopCapacity;
	protected double costLevelUp;
	private boolean actionDid=false;
	public Dwelling() {
		this.type = "Habitation";
		this.neededRessource=new ArrayList<Ressource>();
		this.neededRessource.add(new Iron(400));
		this.neededRessource.add(new Sand(1000));
		this.PopCapacity=(int) Math.exp(this.actualLevel+3);
		this.costLevelUp=20;
		
		this.constructionTime=2;
		this.remainingTime=this.constructionTime;
		this.energyNeeded=0;
		
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public Dwelling(ArrayList<? extends Ressource > needed) {
		this.type = "Habitation";
		this.neededRessource=new ArrayList<Ressource>();
		this.neededRessource.addAll(needed);
		this.PopCapacity=(int) Math.exp(this.actualLevel+3);
		this.costLevelUp=20;
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void levelUp() {
		if(this.actualLevel>=4)
			return;
		
		this.actualLevel++;
		this.PopCapacity=this.calculatePopCapacity(this.actualLevel);
		this.actionDid=false;
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public void LevelDown() {
	

	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	private int calculatePopCapacity(int level) {
		return (int)(Math.exp(7+level)/2);
		
	}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	@Override
	public void doBuildingAction(Stat stat, Tile tile) {
		// TODO Auto-generated method stub


		if(this.actionDid)
			return;
		
		int capacity=stat.getPop().getCapacityNumber();

		if(this.actualLevel>1)
		stat.getPop().setCapacityNumber(   capacity+ (int) this.PopCapacity-this.calculatePopCapacity(this.actualLevel-1)  );
		
		if(this.actualLevel==1)
		stat.getPop().setCapacityNumber(capacity+(int)this.PopCapacity);
		
		this.actionDid=true;
			
		
		
	}
	
}