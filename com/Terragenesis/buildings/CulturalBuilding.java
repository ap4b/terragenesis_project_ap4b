package com.Terragenesis.buildings;

import com.Terragenesis.Mapping.Tile;
import com.Terragenesis.buildings.civil.*;
import com.Terragenesis.functions.Ftool;
import com.Terragenesis.players.Stat;
import com.Terragenesis.ressource.Ressource;

public class CulturalBuilding extends CivilBuilding{
	
	public CulturalBuilding()
	{
		super();
		this.type = "Culture";
		this.numberPopulation=0;
		this.constructionTime=3;
		this.remainingTime=this.constructionTime;
		this.costLevelUp=this.level.getCostLevelUp();
		this.level=new CivilLevel_I();

	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	public CulturalBuilding(Civil level)
	{
		super();
		this.type = "Culture";
		this.numberPopulation=0;
		this.constructionTime=3;
		this.remainingTime=this.constructionTime;
		this.costLevelUp=this.level.getCostLevelUp();
		this.level=level;

	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void doBuildingAction(Stat stat, Tile tile) {
		
		if( !(stat.consumEnergy(this.energyNeeded)) ) {
			return ;
		}
		
		double F= Ftool.squareF(this.numberPopulation,-1 , this.level.getPop()*2,0 );
			  F/= Ftool.squareF(this.level.getPop(),-1 , this.level.getPop()*2,0 );
		
		stat.setCulture(stat.getCulture() + this.level.research()*F );
		
		
	}



}
