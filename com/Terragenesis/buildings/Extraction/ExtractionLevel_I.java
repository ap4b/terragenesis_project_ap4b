package com.Terragenesis.buildings.Extraction;

public class ExtractionLevel_I implements Extraction{
	
	public double extract() {
		return Extraction.num;
	}
	
	public double getCostLevelUp() {
		return 1*Extraction.costLevel;
	}
	public double getPop() {
		return 1*Extraction.nbPop;
	}

}
