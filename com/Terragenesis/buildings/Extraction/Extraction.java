package com.Terragenesis.buildings.Extraction;

public interface Extraction {
	
	static double num=700;
	static double costLevel=50;
	static double nbPop=100;
	
	public double extract();
	public double getCostLevelUp();
	public double getPop();
}
