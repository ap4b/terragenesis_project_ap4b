package com.Terragenesis.buildings.Extraction;

public class ExtractionLevel_II implements Extraction{
	
	public double extract() {
		return 2*Extraction.num;
	}
	public double getCostLevelUp() {
		return 3*Extraction.costLevel;
	}
	public double getPop() {
		return 2*Extraction.nbPop;
	}
}
