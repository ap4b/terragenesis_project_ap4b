package com.Terragenesis.buildings.civil;

import com.Terragenesis.buildings.Extraction.Extraction;

public class CivilLevel_III  implements Civil{

	public double research() {
		
		return 3*Civil.num;
	}
	public double getCostLevelUp() {
		return 3*Civil.costLevel;
	}
	public double getPop() {
		return 3*Extraction.nbPop;
	}
}
