package com.Terragenesis.buildings.civil;

import com.Terragenesis.buildings.Extraction.Extraction;

public class CivilLevel_IV  implements Civil{

	public double research() {
		
		return 4*Civil.num;
	}
	public double getCostLevelUp() {
		return 4*Civil.costLevel;
	}
	public double getPop() {
		return 4*Extraction.nbPop;
	}
}
