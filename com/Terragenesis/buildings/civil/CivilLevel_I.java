package com.Terragenesis.buildings.civil;

import com.Terragenesis.buildings.Extraction.Extraction;

public class CivilLevel_I implements Civil{

	public double research() {
		
		return 1*Civil.num;
	}
	
	public double getCostLevelUp() {
		return 1*Civil.costLevel;
	}
	public double getPop() {
		return 1*Extraction.nbPop;
	}

}
