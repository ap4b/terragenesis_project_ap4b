package com.Terragenesis.buildings.civil;

import com.Terragenesis.buildings.Extraction.Extraction;

public class CivilLevel_II implements Civil{

	public double research() {
		
		return 2*Civil.num;
	}
	public double getCostLevelUp() {
		return 2*Civil.costLevel;
	}
	public double getPop() {
		return 2*Extraction.nbPop;
	}
}