package com.Terragenesis.players;

public class Player {
	
	protected int maxActionPoint;
	protected int actionPoint;
	protected Stat playerStat;
	
	
	
	
	public Player() {
		super();
		this.maxActionPoint = 25;
		this.actionPoint = 25;
		this.playerStat = new Stat();
		
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public Player(int maxActionPoint, int actionPoint, Stat playerStat) {
		super();
		this.maxActionPoint = maxActionPoint;
		this.actionPoint = actionPoint;
		this.playerStat = playerStat;
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	
	
	public void resetActionPoints() {
		actionPoint = maxActionPoint;
	}
	
	public void useAction(int nbPoints) {
		actionPoint -= nbPoints;
	}
	
	public int  getMaxActionPoint() 				  { return maxActionPoint;					}
	public void setMaxActionPoint(int maxActionPoint) { this.maxActionPoint = maxActionPoint; 	}
	public int  getActionPoint   ()					  { return actionPoint; 					}
	public void setActionPoint   (int actionPoint) 	  {	this.actionPoint = actionPoint; 		}
	public Stat getPlayerStat	 () 				  {	return playerStat;						}

	

	
	
	
	

}
