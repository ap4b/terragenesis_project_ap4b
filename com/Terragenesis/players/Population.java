package com.Terragenesis.players;

import java.util.ArrayList;

import com.Terragenesis.ressource.DrinkingWater;
import com.Terragenesis.ressource.Foodstuff;

public class Population {
	
 	protected double happiness;
	protected double foodconsumption;
	protected double drinkingWaterConsumption;
	protected int CapacityNumber;
	protected int numberPopulation;

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public Population() {
		this.drinkingWaterConsumption=1;
		this.foodconsumption=1;
		this.numberPopulation=1000;
		this.CapacityNumber=this.numberPopulation;
		this.happiness=10;
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public Population(double happiness, double foodconsumption, double drinkingWaterConsumption ,int numberPopulation) {
		this.drinkingWaterConsumption=drinkingWaterConsumption;
		this.foodconsumption=foodconsumption;
		this.numberPopulation=numberPopulation;
		this.CapacityNumber=this.numberPopulation;
		this.happiness=happiness;
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	
	public void evolvePopulation( Stat stat) {
		
		this.numberPopulation*=1.2;
		
		if(this.numberPopulation>this.CapacityNumber) {
			this.numberPopulation=this.CapacityNumber;
		}
		
		double PopStarve = this.PopulationStarvation(stat);
		if(PopStarve<0) {
		    this.numberPopulation+=PopStarve;
			return;
		}
		
		
		
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public double PopulationStarvation(Stat stat) { //get the number of people that will die
		
		ArrayList<Integer> index = stat.getAllTypeRessource(Foodstuff.class.getSimpleName() );
		int i=0;
		double food=0, water=0;
		
		for(i=0; i<index.size();i++) {
			food+=stat.getRessourceStat().get(index.get(i)).getNumber();
		}
		
		i=stat.getRessourceIndex( new DrinkingWater() );
		water+= stat.getRessourceStat().get(i).getNumber();
		
		double PopStarveFeed=0, PopStarveWater=0;
		
		PopStarveFeed    = (food /this.foodconsumption         ) - this.numberPopulation;
		PopStarveWater = (water/this.drinkingWaterConsumption) - this.numberPopulation;
		
		if( PopStarveFeed>0 && PopStarveWater>0)
			return 0;
		
		
		if( PopStarveFeed<PopStarveWater ) {
				return PopStarveFeed;
		
		} else {
				return PopStarveWater;
		 }	
		
		
		
	}

	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public double getHappiness      		 () 					  		   { return happiness;											}
	public void   setHappiness      		 (double happiness) 	  		   { this.happiness = happiness;								}
	public double getFoodconsumption		 () 					  		   { return foodconsumption;									}
	public void   setFoodconsumption         (double foodconsumption) 		   { this.foodconsumption = foodconsumption;					}
	public double getDrinkingWaterConsumption() 					  		   { return drinkingWaterConsumption;							}
	public void   setDrinkingWaterConsumption(double drinkingWaterConsumption) { this.drinkingWaterConsumption = drinkingWaterConsumption;	}
	public int getNumberPopulation		 () 							   { return numberPopulation;									}
	public void   setNumberPopulation		 (int numberPopulation) 		   { this.numberPopulation = numberPopulation;					}
	public int getCapacityNumber		 () 							   	   { return CapacityNumber;										}
	public void   setCapacityNumber		 (int CapacityNumber) 		       { this.CapacityNumber = CapacityNumber;						}
	
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	@Override
	public String toString() {
		return "Population [happiness=" + happiness + ", foodconsumption=" + foodconsumption
				+ ", drinkingWaterConsumption=" + drinkingWaterConsumption + ", CapacityNumber=" + CapacityNumber
				+ ", numberPopulation=" + numberPopulation + "]";
	}



	

}
