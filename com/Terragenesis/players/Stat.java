package com.Terragenesis.players;

import java.util.ArrayList;
import java.util.HashMap;

import com.Terragenesis.ressource.*;
import com.Terragenesis.Mapping.Coord;
import com.Terragenesis.buildings.*;


public class Stat {

	protected ArrayList<Building>  buildingStat;				 // player's buildings
	protected ArrayList<Coord> coord= new ArrayList<Coord>();	// coord of each building
	protected ArrayList<Ressource> ressourceStat;			   // player's ressources
	
	
	protected Population pop;
	
	protected double culture;
	protected double science;
	//protected double TotalFood;
	
	
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		public Stat() {
			super();
			this.buildingStat = new ArrayList<Building>();
			this.ressourceStat= new ArrayList<Ressource>();
			
			this.ressourceStat.add(new Iron(10000));
			this.ressourceStat.add(new Sand(10000));
			this.ressourceStat.add(new Energy(10000));
			this.ressourceStat.add(new Wheat(10000));
			this.ressourceStat.add(new DrinkingWater(10000));

			
			this.pop = new Population();
			this.culture = 100;
			this.science = 100;
		//	this.TotalFood=0;
		}
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		public Stat(ArrayList<Ressource> ressource) {
			super();
			this.ressourceStat=ressource;

			this.buildingStat = new ArrayList<Building>();
			
			this.pop = new Population();
			this.culture = 0;
			this.science = 0;
		//	this.TotalFood=0;
		}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public Stat(ArrayList<Building> buildingStat, Population pop, ArrayList<Ressource> ressource, double culture, double science) {
		super();
		this.ressourceStat= ressource;
		this.buildingStat = buildingStat;
		this.pop = pop;
		this.culture = culture;
		this.science = science;
	//  this.TotalFood=food;

		
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void addBuidlings(ArrayList <? extends Building > list) {
		this.buildingStat.addAll(list);
	}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void addBuidling(Building build) {
		this.buildingStat.add(build);
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public  ArrayList<Building> getTypeOfBuilding(String nameBuilding) { //get player's buildings of one type
		
		ArrayList<Building> list=new ArrayList<Building>();
		int i;
		
		for(i=0; i<this.buildingStat.size(); i++)
		{
			if(this.buildingStat.get(i).getClass().getSimpleName().equals(nameBuilding))
				list.add(this.buildingStat.get(i));
		}
		return list;
	}
		
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public int getRessourceIndex(Ressource r) { // get the ressource index in the Arraylist ressources 
		
		int i=-1;
		boolean stop=false;
		
		do{
			i++;
			if( r.getClass().getSimpleName().equals( this.ressourceStat.get(i).getClass().getSimpleName() ) ){
				stop=true;
			}
		}while( i<this.ressourceStat.size() && !stop );
		
		return i;
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public ArrayList<Integer> getAllTypeRessource(String r) { //get 
			
			int i=0;
			ArrayList<Integer> index = new ArrayList<Integer>();
			
			do{
			
				if( r.equals( this.ressourceStat.get(i).getClass().getSuperclass().getSimpleName() ) ){
					index.add(i);
				}
			i++;
			}while( i<this.ressourceStat.size() );
			
			return index;
		}
		
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void constructBuilding(Building b) { // when constructing a building subtract buidling's resources to ours resources
		
		for(int i=0; i<b.getNeededRessource().size();i++) {
			
			this.ressourceStat.get( this.getRessourceIndex(b.getNeededRessource().get(i)) ).soustract(b.getNeededRessource().get(i).getNumber());	
		}
	
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void PopulationConsuming() { // applique la consommation de ressource de la population 
		
		ArrayList<Integer> index = this.getAllTypeRessource(Foodstuff.class.getSimpleName() );
		int i=0;
		double food=0, water=0;
		food= this.pop.getCapacityNumber()*this.pop.getFoodconsumption();
		
		for(i=0; i<index.size();i++) {
			if( this.getRessourceStat().get( index.get(i) ).getNumber()>food ) {
				this.getRessourceStat().get( index.get(i) ).extract(food);
				food=0;
				
			} else { 	  this.getRessourceStat().get(index.get(i)).setNumber(0);
			   		food-=this.getRessourceStat().get(index.get(i)).getNumber();
			}
		}
		
		
		i=this.getRessourceIndex( new DrinkingWater() );
		water= this.pop.getCapacityNumber()*this.pop.getDrinkingWaterConsumption();
		
		if( water<this.getRessourceStat().get(i).getNumber() ) {
				 this.getRessourceStat().get(i).extract(water);
		} else {
				 this.getRessourceStat().get(i).setNumber(0);
		}
	}
	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	public void evolvePopulation() {  // function used to leverage the population number
		
		this.pop.evolvePopulation(this);
	}

	
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public boolean consumEnergy(int a) {
		int index= this.getRessourceIndex( new Energy());
		
		if( this.ressourceStat.get(index).getNumber()-a>0 ) {
			this.ressourceStat.get(index).extract(a);
			return true;
		}
		return false;
	}
	
	
	
	public double getCulture() 			 	 { return culture;		   }
	public void   setCulture(double culture) { this.culture = culture; }
	public double getScience() 				 { return science;   	   }
	public void   setScience(double science) { this.science = science; }
/*	public double getTotalFood() 			 { return this.TotalFood;  }
	public void   setTotalFood(double food)	 { this.TotalFood=food;    }*/
	public Population getPop() 				 { return pop;			   }
	public void   setPop(Population pop) 	 { this.pop = pop;  	   }
	/*public double getEnergy() 				 { return energy;		   }
	public void   setEnergy(double energy) 	 { this.energy = energy;   }*/

	public ArrayList<Ressource> getRessourceStat() 									 { return ressourceStat;				}
	public void 				setRessourceStat(ArrayList<Ressource> ressourceStat) { this.ressourceStat = ressourceStat;	}
	public ArrayList<Building>  getBuildingStat() 									 { return buildingStat;					}
	public void 				setBuildingStat(ArrayList<Building> buildingStat) 	 { this.buildingStat = buildingStat;	}
	public ArrayList<Coord> 	getCoord() 											 { return coord; 						}

	
	

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


	
	
	
}
